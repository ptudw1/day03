<!DOCTYPE html>
<html>
	<head>
		<title>Register</title>
        <link rel="stylesheet" type="text/css" href="student.css"/>
		<?php 
			header('Content-Type: text/html; charset=utf-8');
			date_default_timezone_set("Asia/Bangkok");
			$gender_arr = array("Nam", "Nữ");
			$major_arr = array("Khoa học máy tính"=>"MAT", "Khoa học vật liệu"=>"KDL");
		?>
		<div class="container">
			<form method="post">
			
				<div class="form-group">
					<div class="col-label">
						<label for="username">Họ và tên</label>
					</div>
			
					<div class="col-input">
						<input type="text" name="name" /> 
					</div>
				</div>
			
			
				<div class="form-group">
					<div class="col-label">
						<label for="gender">Giới tính</label>
					</div>
				
					
					<div class="col-input">
						<?php
							for($i = count($gender_arr) - 1; $i >= 0; $i--) {
								echo"<label class=\"radio-item\" for=$i>$gender_arr[$i]";
								echo"<input type=\"radio\" name=\"gender\" value=$i />";
								echo"<span class=\"radio-mark\"></span>";
								echo"</label>";
							}
						?>
					</div>
				</div>
	
			
				<div class="form-group">
					<div class="col-label">
						<label for="major">Phân khoa</label>
					</div>
				
					<div class="col-input">
						<select id="major" name="major">
							<option hidden disabled selected value></option>
							<?php
								foreach($major_arr as $x => $x_value) {
									echo"<option value=$x_value>$x</option>";
								}
							?>
						</select>
					</div>
				</div>
				
				
				<div class="submit">
					<input type="submit" value="Đăng ký">
				</div>
		    </form>
		</div>
	</body>
</html>